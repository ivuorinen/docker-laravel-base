FROM ubuntu AS base

RUN apt-get update

RUN apt-get install -y \
			git \
			vim \
			software-properties-common \
			python-software-properties \
            curl

# Install nginx
RUN apt-get install -y nginx

# Install nodejs
RUN cd /opt && curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y nodejs

EXPOSE 80 443